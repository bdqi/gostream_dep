1. 安装

        sudo docker pull mongo
        
2. 配置
        
      - 启动
            
            数据存储在本机的/opt/mongo/data/目录下，先不开启认证
            sudo docker run -itd --name mongo -v /opt/mongo/data/:/data/db/ -p 27017:27017 mongo

      - 用户密码设置
      
            进入容器
            sudo docker exec -it mongo bash 
            查看mongo版本
            mongo --version
            连接mongo
            mongo 127.0.0.1:27017
            设置管理员账号
            use admin
            db.createUser({ user:'admin',pwd:'topsci123!',roles:[ { role:'userAdminAnyDatabase', db: 'admin'}]});
            设置视频管理系统账号
            use video
            db.createUser({ user:'video',pwd:'F8w#90jd',roles:[ { role:'readWrite', db: 'video'}]});
            写入默认数据
            db.global.insert({"key":"merge_videos_exp","var":NumberInt(2)});
            db.catalogs.insert({"id":"root","parent":"","name":"根目录"});
      - 以认证方式启动
      
            先退出容器，然后停止容器
            sudo docker rm -f mongo
            以认证方式启动
            sudo docker run -itd --name mongo -v /opt/mongo/data/:/data/db/ -p 27017:27017 mongo  --auth
            
3. 连接
    
            
作用：
	
>管理流媒体服务、工具服务，并对外提供接口

>单节点或多节点部署


安装条件：
	
>linux 64bit系统

>已安装docker和docker-compose

>需要提前安装mongodb和etcd集群(安装说明略)

1. 启动mongo和etcd
	
	mongo docker镜像及单机版参考配置，见下面链接

	[https://106.38.108.70:4080/index.php?mod=shares&sid=bUoyQTlUelp5ZnVXQUFrMGxLM2NNRndOUHlmVno1RktZSjFQcFE](https://106.38.108.70:4080/index.php?mod=shares&sid=bUoyQTlUelp5ZnVXQUFrMGxLM2NNRndOUHlmVno1RktZSjFQcFE)
	
	etcd集群 略

2. mongodb写入初始化数据

		配置好video数据库的可读写用户名/密码
		
		在video数据库中写入如下两条数据

		db.global.insert({"key":"merge_videos_exp","var":NumberInt(2)});

		db.catalogs.insert({"id":"root","parent":"","name":"根目录"});

	
2. 修改配置文件

		cd manager/
		
		根据docker-compose.yml中注释进行修改
        
2. 程序安装
	
		sudo chmod +x *.sh
	
        ./install.sh
        
5. 启动及开机启动
        
        ./start.sh
      
6. 停止
    
        ./stop.sh

7. 验证是否启动成功

		执行
		docker ps --filter ancestor=gostream/docker-apps-manager:latest

		看到如下结果，且STATUS是Up状态则正常启动
		CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS              PORTS               NAMES
		cf8f2ba6b13e        gostream/docker-apps-manager:latest   "/apps-manager -Disc…"   51 seconds ago      Up 50 seconds                           apps-managers

8. 验证接口是否可访问

		执行下面命令(若配置文件中端口更改，则下面命令端口改为配置文件API端口)

		curl http://127.0.0.1:28765/manager/streamservers
		
		无错误返回则成功，若有错误，则检查端口是否配置正确

		看到如下结果，则成功	
		{"list":[{"serviceid":"U2VydmljZVR5cGVTdHJlYW1fMTkyLjE2OC40Ljk5OjE4MTgx","servicename":"ServiceTypeStream","ip":"192.168.4.99","port":"18181","online":"true","time":1609930054},{"serviceid":"U2VydmljZVR5cGVTdHJlYW1fMC4wLjAuMC4wOjEyMzQ","servicename":"ServiceTypeStream","ip":"0.0.0.0.0","port":"1234","online":"false","time":1609930041}]}
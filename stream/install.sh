#!/bin/bash
CWD=$(cd "$(dirname $0)";pwd)

#license
sudo mkdir -p /var/lic/apps-stream
sudo cp ${CWD}/lic/lic.lic /var/lic/apps-stream/
#nginx
sudo mkdir -p /var/opt/apps-stream/nginx/config
sudo mkdir -p /var/opt/apps-stream/nginx/config/ssl
sudo mkdir -p /var/opt/apps-stream/nginx/logs
sudo cp ${CWD}/nginx.conf /var/opt/apps-stream/nginx/config/
sudo cp ${CWD}/ssl/*.pem /var/opt/apps-stream/nginx/config/ssl/
sudo cp ${CWD}/ssl/*.key /var/opt/apps-stream/nginx/config/ssl/
#docker images
sudo docker load -i ${CWD}/docker-apps-stream.tar
sudo docker load -i ${CWD}/nginx.tar

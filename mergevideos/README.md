作用：
	
>一些辅助工具

>单节点部署


安装条件：
	
>linux 64bit系统

>已安装docker和docker-compose

>硬盘空间大一点，一些临时存储需要空间

	
3. 修改配置文件

		cd mergevideos/
		
		根据docker-compose.yml中注释进行修改
        
4. 程序安装
	
		sudo chmod +x *.sh
	
        ./install.sh

		若出现open XXXX/gostream_dep/mergevideos/nginx.tar: no such file or directory
			- 若系统联网，则忽略此错误，会在启动时自动下载nginx的docker
			- 若为离线，则需要下载nginx的docker镜像，放到此目录下，重新执行此步骤
				- nginx镜像获取方法(仅供参考)
					- sudo docker pull nginx
					- sudo docker save -o nginx.tar nginx:latest
				-或直接去下面地址获取
					https://106.38.108.70:4080/index.php?mod=shares&sid=bUoyQTlUelp5ZnVXQUFNNXhLclpNd3NETXliWG1KWWJhTXhQcWc
        
5. 启动及开机启动
        
        ./start.sh
      
6. 停止
    
        ./stop.sh

7. 验证是否启动成功

		执行
		docker ps --filter ancestor=gostream/docker-apps-mergevideos:latest

		看到如下结果，且STATUS是Up状态则正常启动
		CONTAINER ID        IMAGE                                     COMMAND                  CREATED             STATUS              PORTS               NAMES
		18332d751abf        gostream/docker-apps-mergevideos:latest   "/apps-mergevideos -…"   30 seconds ago      Up 30 seconds                           apps-mergevideos

8. 验证接口是否可访问

		执行下面命令(若配置文件中端口更改，则下面命令端口改为nginx监听端口)

		curl http://127.0.0.1:51818/mergevideos/keepalive
		
		无错误返回则成功，若有错误，则检查端口是否配置正确

		看到如下结果，则成功	
		{"issetparam":false,"exp":-1,"mergevideos_callback_url":""}
